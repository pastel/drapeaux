import tkinter as tk

canvas_width = 450
canvas_height = 300


class Application(tk.Frame):
    def __init__(self, master=None):
        tk.Frame.__init__(self, master)
        self.grid()
        self.createWidgets()

    def createWidgets(self):
        self.flag = tk.Canvas(self, width=canvas_width, height=canvas_height)
        self.flag.grid(row=0, column=0, columnspan=6)
        tk.Button(self, text="France",
                  command=self.draw_france).grid(row=1, column=0)
        tk.Button(self, text="Japan",
                  command=self.draw_japan).grid(row=1, column=1)
        tk.Button(self, text="Sweden",
                  command=self.draw_sweden).grid(row=1, column=2)
        tk.Button(self, text="Esperanto",
                  command=self.draw_esperanto).grid(row=1, column=3)
        tk.Button(self, text="Ghana",
                  command=self.draw_ghana).grid(row=1, column=4)
#         tk.Button(self, text="Maldives",
#                   command=self.draw_maldives).grid(row=1, column=5)

    def draw_star(self, star_width, star_height, color):
        verts = [0, 30, 30, 30, 40, 0, 50, 30, 80, 30, 55,
                 50, 65, 80, 40, 60, 15, 80, 25, 50]
        for i in range(0, len(verts), 2):
            verts[i] += star_width
            verts[i+1] += star_height
        self.flag.create_polygon(verts, fill=color)

    def draw_france(self):
        self.flag.delete("all")
        canvas_height = canvas_width/3*2
        self.flag.create_rectangle(0, 0,
                                   canvas_width, canvas_height,
                                   fill='white')
        self.flag.create_rectangle(0, 0,
                                   canvas_width/3, canvas_height,
                                   fill='blue')
        self.flag.create_rectangle(2*canvas_width/3, 0,
                                   canvas_width, canvas_height,
                                   fill='red')

    def draw_japan(self):
        self.flag.delete("all")
        canvas_height = canvas_width/3*2
        height_white = canvas_height/8
        width_white = (canvas_width-canvas_height*3/4)/2
        self.flag.create_rectangle(0, 0, canvas_width, canvas_height,
                                   fill='white')
        self.flag.create_oval(width_white, height_white,
                              canvas_width-width_white,
                              canvas_height-height_white,
                              fill='red')

    def draw_sweden(self):
        self.flag.delete("all")
        canvas_height = canvas_width/8*5
        w_part = canvas_width/16
        h_part = canvas_height/10
        self.flag.create_rectangle(0, 0,
                                   canvas_width, canvas_height,
                                   fill='yellow')
        self.flag.create_rectangle(0, 0,
                                   5*w_part, 4*h_part, fill='blue')
        self.flag.create_rectangle(7*w_part, 0,
                                   canvas_width, 4*h_part, fill='blue')
        self.flag.create_rectangle(0, 6*h_part,
                                   5*w_part, canvas_height, fill='blue')
        self.flag.create_rectangle(7*w_part, 6*h_part,
                                   canvas_width, canvas_height, fill='blue')

# can't manage to place the star on the right place + bad dimensions for it.
    def draw_ghana(self):
        self.flag.delete("all")
        canvas_height = canvas_width/3*2
        self.flag.create_rectangle(0, 0,
                                   canvas_width, canvas_height/3,
                                   fill='red')
        self.flag.create_rectangle(0, canvas_height/3,
                                   canvas_width, canvas_height/3*2,
                                   fill='yellow')
        self.flag.create_rectangle(0, canvas_height/3*2,
                                   canvas_width, canvas_height,
                                   fill='green')
#         self.draw_star(0, 0)
        self.draw_star(canvas_width/2-40, canvas_height/2-40, 'black')

    def draw_esperanto(self):
        self.flag.delete("all")
        canvas_height = canvas_width/3*2
        self.flag.create_rectangle(0, 0,
                                   canvas_width, canvas_height,
                                   fill='green')
        self.flag.create_rectangle(0, 0,
                                   canvas_width/3, canvas_height/2,
                                   fill='white')
        self.draw_star(20, 20, 'green')
'''
# create_arc is a pain in the ass to use.

    def draw_maldives(self):
        self.flag.delete("all")
        canvas_height = canvas_width/3*2
        self.flag.create_rectangle(0, 0,
                                   canvas_width, canvas_height,
                                   fill='red')
        self.flag.create_rectangle(canvas_width/6, canvas_height/4,
                                   canvas_width/6*5, canvas_height/4*3,
                                   fill='green')
#         self.flag.create_arc(canvas_width/12*5, canvas_height/16*5,
#                              canvas_height/12*7, canvas_height/16*11,
#                              fill='white')
        self.flag.create_arc(0, 0, canvas_width, canvas_height,
                             fill='white')
'''



root = tk.Tk()
app = Application(master=root)
app.master.title("Le dessinateur de drapeaux")
app.mainloop()
